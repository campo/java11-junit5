package nl.marimba.kotlin.io

import java.io.File
import java.util.*
import java.util.function.Predicate
import java.util.stream.Collectors

fun readFirstLines(file: File, n: Long = 1): List<String> {
    val res: List<String> = file.bufferedReader()
        .lines()
        .limit(n)
//        .peek(it -> println(it))
        .collect(Collectors.toList())
    return res
}

fun firstMatchIn(file: File, n: Long = 1, cond: Predicate<String>): String {
    val res: Optional<String> = file.bufferedReader()
        .lines()
        .peek { println(it) }
        .limit(n)
        .filter(cond)
        .findFirst()
    return res.orElse("")
}


