package nl.marimba.kotlin.io

import kotlinx.serialization.*
import kotlinx.serialization.json.*

@Serializable
data class SchemaInfo(
    val tns: String,
    val version: String
)

class SerializeJson {

    fun buildSimple(): String {
        val si = SchemaInfo(tns = "hello world", version = "1.2.3")
        return Json.encodeToString(si)
    }

    fun buildList(): String {
        val siList = arrayOf(
            SchemaInfo(tns = "hello first three worlds", version = "1.2.3"),
            SchemaInfo(tns = "hello worlds", version = "4.5.6"),
        )
        return Json.encodeToString(siList)
    }
}
