package nl.marimba.kotlin.io

import org.w3c.dom.Element
import java.io.File
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

data class XmlSchemaInfo(
    val tns: String,
    val version: String
)

class ReadXmlFile() {

    fun getSchemaInfo(root: Element): XmlSchemaInfo {
        root.normalize()
        return XmlSchemaInfo(
            tns = root.getAttribute("targetNamespace"),
            version = root.getAttribute("version")
        )
    }

    fun docBuilder(): DocumentBuilder {
        val df = DocumentBuilderFactory.newInstance()
        df.setNamespaceAware(true)
        return df.newDocumentBuilder()
    }

    fun getSchemaInfo(file: File): XmlSchemaInfo {
        val doc = docBuilder().parse(file)
        return getSchemaInfo(doc.documentElement)
    }

    fun getSchemaInfo(xml: String): XmlSchemaInfo {
        val xs = xml.byteInputStream()
        val doc = docBuilder().parse(xs)
        return getSchemaInfo(doc.documentElement)
    }

}
