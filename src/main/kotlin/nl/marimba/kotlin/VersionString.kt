package nl.marimba.kotlin

data class VersionString(val versionSpec: String) : Comparable<VersionString> {

    init {
        require(versionSpec.matches(Regex("^\\d+(\\.\\d+)*\$")))
    }

    val components by lazy { initList() }

    fun initList(): List<UInt> = versionSpec.split('.').map { it.toUInt() }

    override fun compareTo(other: VersionString): Int {
        return compareLists(components, other.components)
    }

    override fun equals(other: Any?): Boolean {
        if (other is VersionString) {
            return compareLists(components, other.components) == 0
        }
        return super.equals(other)
    }
}

fun compareLists(left: List<UInt>, right: List<UInt>): Int =
    when {
        // both sides are exhausted means they are equal
        // either side exhausted means the other side wins
        left.isEmpty() && right.isEmpty() -> 0
        left.isEmpty() && right.isNotEmpty() -> -1
        right.isEmpty() && left.isNotEmpty() -> 1

        // not done yet, does the next component decide?
        left[0] != right[0] -> left[0].toInt() - right[0].toInt()

        // no, recurse
        else ->
            compareLists(left.subList(1, left.size), right.subList(1, right.size))
    }

