package nl.marimba.kotlin.regex

fun isMdHead1(s: String): Boolean {

    val token = """^\s*#[^#]+""".toRegex()
    return token.find(s) != null
}

fun findReferences(template: String): List<String> {
    println("Scanning '$template'")
    Regex("\\$(\\d+)")
        .findAll(template)
        .map {
            println(it.groupValues)
        }
    return emptyList()
}
