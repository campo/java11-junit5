package nl.marimba.kotlin.regex

class DynamicMatcher(val pattern: String, val replacement: String) {

    val p = pattern.toRegex(RegexOption.IGNORE_CASE)

    fun matches(str: String): Boolean {
        return p.matches(str)
    }

    fun replaced(str: String): String? {
        return try {
            str.replace(Regex(pattern), replacement)
        } catch (e: Exception) {
            null
        }
    }
}
