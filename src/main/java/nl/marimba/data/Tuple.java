package nl.marimba.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Tuple<T1, T2>  {

    final T1 l;

    final T2 r;

}
