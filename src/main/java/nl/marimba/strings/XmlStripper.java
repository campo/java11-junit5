package nl.marimba.strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface XmlStripper {

    Pattern xmlDecl = Pattern.compile("(?is)<\\?xml[^>]*\\??>");

    static String removeXmlDeclaration(String xml) {
        Matcher m = xmlDecl.matcher(xml);
        return m.replaceFirst("").stripLeading();
    }

}
