package nl.marimba.euler;

import nl.marimba.data.Tuple;

import java.util.function.BiFunction;
import java.util.function.IntSupplier;

public class Fibonacci implements IntSupplier {

    static BiFunction<Integer, Integer, Tuple<Integer, Integer>> nextFib =
            (p, c) -> new Tuple<>(c, p + c);

    private int curr = 0;
    private int next = 1;

    @Override
    public int getAsInt() {
        int result = curr + next;
        curr = next;
        next = result;
        return result;
    }
}
