package nl.marimba.euler;

import java.util.stream.IntStream;

/**
 * [Problem 1 - Project Euler](https://projecteuler.net/problem=1)
 */
public class Euler1 {

    private final int max;

    public Euler1() {
        this(10);
    }

    public Euler1(int max) {
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public static void main(String[] args) {
        Euler1 sut1000 = new Euler1(1000);
        Euler1 sut = new Euler1();

        System.out.printf("The sum of all the multiples of 3 or 5 below %d is %d\n",
                sut.max, sut.p1sol1());

        System.out.printf("The sum of all the multiples of 3 or 5 below %d is %d\n",
                sut1000.max, sut1000.p1sol1());

        System.out.printf("The sum of all the multiples of 3 or 5 below %d is %d\n",
                sut1000.max, sut1000.p1sol2());
    }

    /**
     * Simpele iteratieve benadering
     *
     * @return oplossing
     */
    int p1sol1() {
        int result = 0;
        for (int i = 0; i < max; i++) {
            if (i % 3 == 0 || i % 5 == 0) result += i;
        }
        return result;
    }

    /**
     * Simpele iteratieve benadering
     *
     * @return oplossing
     */
    int p1sol2() {
        return IntStream.range(1, max)
                        .filter((i) -> i % 3 == 0 || i % 5 == 0)
                        .reduce(0, Integer::sum);
    }


}
