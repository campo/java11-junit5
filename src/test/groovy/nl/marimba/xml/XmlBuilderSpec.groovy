package nl.marimba.xml

import groovy.xml.MarkupBuilder
import spock.lang.Specification

class XmlBuilderSpec extends Specification {

    def xmlWriter = new StringWriter()
    def xmlMarkup = new MarkupBuilder(xmlWriter)

    def "we can create an element"() {
        when:
        xmlMarkup.movie("the godfather")

        then:
        "<movie>the godfather</movie>" == xmlWriter.toString()
    }

    def "we can create two adjacent elements"() {
        when:
        xmlMarkup.movie("the godfather 1")
        xmlMarkup.movie("the godfather 2")

        then:
        '''
<movie>the godfather 1</movie>
<movie>the godfather 2</movie>
        '''.trim()
            == xmlWriter.toString()
    }
}
