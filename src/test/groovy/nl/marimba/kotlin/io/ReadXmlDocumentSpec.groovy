package nl.marimba.kotlin.io

import org.w3c.dom.Element
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import javax.xml.parsers.DocumentBuilderFactory
import java.nio.charset.StandardCharsets

class ReadXmlDocumentSpec extends Specification {
    @Shared
    def xmldoc1 = """<?xml version="1.0" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           targetNamespace="someNamespace">
</xs:schema>
"""
    @Shared
    def xmldoc2 = """<?xml version='1.0' ?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            targetNamespace="http://mmln.nl/xml/someOtherNamespace"
            version="3.1.4">
</xsd:schema>
"""

    static def parseXml(String xml) {
        def xs = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        def df = DocumentBuilderFactory.newInstance()
        df.setNamespaceAware(true)
        def doc = df.newDocumentBuilder().parse(xs)
        doc.documentElement.normalize()
        doc
    }

    @Unroll
    def "Get document element #qTag"() {
        given:
        def doc = parseXml(xml)

        when:
        Element root = doc.documentElement
        def ns = root.getAttribute("targetNamespace")

        then:
        root.tagName == qTag
        root.localName == "schema"
        ns == tns

        where:
        xml     || qTag         | tns
        xmldoc1 || "xs:schema"  | "someNamespace"
        xmldoc2 || "xsd:schema" | "http://mmln.nl/xml/someOtherNamespace"
    }

    def "Get SchemaInfo from String source"() {
        when:
        def xsi = new ReadXmlFile().getSchemaInfo(xml)

        then:
        xsi.tns == tns
        xsi.version == version

        where:
        xml     || version | tns
        xmldoc1 || ""      | "someNamespace"
        xmldoc2 || "3.1.4" | "http://mmln.nl/xml/someOtherNamespace"

    }
}
