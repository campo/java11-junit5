package nl.marimba.kotlin.io

import spock.lang.Specification

class FilesByLinesSpec extends Specification {

    def input = new File("/usr/share/dict/words")

    def "We can read just the first 10 lines"() {

        when:
        def lines = ReadFilesKt.readFirstLines(input, 10)

        then:
        lines.size() == 10
    }

    def "We can find the first matching line within the first 10 lines"() {

        when:
        def found = ReadFilesKt.firstMatchIn input, 10, {
            it.toLowerCase().startsWith("aardw")
        }

        then:
        found == "aardwolf"
    }

    def "We cannot find a matching line if the limit is too low"() {

        when:
        def found = ReadFilesKt.firstMatchIn input, 8, {
            it.toLowerCase().startsWith("aardw")
        }

        then:
        found == ""
    }


}

