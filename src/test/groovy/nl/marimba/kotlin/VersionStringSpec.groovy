package nl.marimba.kotlin

import spock.lang.Specification

class VersionStringSpec extends Specification {

    def "Accept valid version"() {
        when:
        def sut = new VersionString(vvv)

        then:
        sut

        where:
        vvv << ["1", "0", "1.2", "0.0.1", "0.99.001"]
    }

    def "Refuse invalid version"() {
        when:
        def sut = new VersionString(vvv)

        then:
        thrown(IllegalArgumentException)

        where:
        vvv << ["1.", "", ".2", "0.a.1", "0.99..001"]
    }

    def "Order from left to right by numeric content"() {
        given:
        def vBigger = new VersionString(bigger)
        def vSmaller = new VersionString(smaller)

        expect:
        vBigger > vSmaller
        vSmaller < vBigger
        vSmaller != vBigger

        where:
        bigger      | smaller
        "0.1.1"     | "0.1.0"
        "0.1.1"     | "0.1.0.0"
        "0.2"       | "0.1.1"
        "3"         | "2.999"
        "30"        | "29.999"
        "0.2456.0"  | "0.300.1"
        "0.10.1300" | "0.10.150"
        "1"         | "0.0.1"

    }

    def "Equality means identical numbers"() {
        given:
        def vLeft = new VersionString(left)
        def vRight = new VersionString(right)

        expect:
        vLeft == vRight

        where:
        left        | right
        "0.1.1"     | "0.1.1"
        "0.1.0.0"   | "0.1.0.0"
        "3"         | "3"
        "2.999"      | "2.999"
        "0.2456.0"  | "0.2456.0"
        "0.300.1"   | "0.300.1"
        "0.10.1300" | "0.10.1300"

    }
}
