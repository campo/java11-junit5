package nl.marimba.kotlin.regex

import spock.lang.Specification
import spock.lang.Unroll


class KotlinRegexSpec extends Specification {

    @Unroll
    def "Finding a level 1 head in Markdown"() {
        expect:
        RegexFunKt.isMdHead1(s)

        where:
        s << [
            "# Dit is een level 1 header",
            "  #    Dit is ook een level 1 header"
        ]
    }

    def "Ignoring a level 2 head in Markdown"() {
        expect:
        !RegexFunKt.isMdHead1(s)

        where:
        s << [
            "## Dit is een level 2 header",
            "  ##    Dit is ook een level 2 header"
        ]
    }

    def "Ignoring other content in Markdown"() {
        expect:
        !RegexFunKt.isMdHead1("Dit is geen header")
    }

}
