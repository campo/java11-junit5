package nl.marimba.kotlin.regex

import spock.lang.Specification

class RegexReferencesSpec extends Specification {
    def "FindReferences"() {
        when:
        def l = RegexFunKt.findReferences("1=\$1;2=\$2;10=\$10")

        then:
        l.size() > 0
    }

}
