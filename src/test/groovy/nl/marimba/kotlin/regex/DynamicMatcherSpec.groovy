package nl.marimba.kotlin.regex


import spock.lang.Specification

class DynamicMatcherSpec extends Specification {

    public static final String RELEASE_PATTERN = '^release/(?:\\d*)(\\d{2})\\.([0-9a-zA-z]+)'
    public static final String FEATURE_PATTERN = '^feature/(?:[a-zA-z0-9]+-)?(\\d+)'

    def "Matches develop properly"() {

        given:
        def sut = new DynamicMatcher('^develop$', "develop")

        expect:
        sut.matches(branch) == expected

        where:
        branch     || expected
        "develop"  || true
        "Develop"  || true
        "developp" || false
    }

    def "Matches release branch properly"() {

        given:
        def sut = new DynamicMatcher(RELEASE_PATTERN, "r\$1")

        expect:
        sut.matches(branch) == expected

        where:
        branch             || expected
        "release/"         || false
        "release/2021.21"  || true
        "release/2021.21a" || true
    }

    def "Substitutes release branch properly"() {

        given:
        def sut = new DynamicMatcher(RELEASE_PATTERN, "r\$1\$2")

        when:
        def result = sut.replaced(branch)

        then:
        result == expected

        where:
        branch             || expected
        "release/"         || "release/"
        "release/2021.32"  || "r2132"
        "release/21.32"    || "r2132"
        "release/2021.32a" || "r2132a"
    }

    def "Matches feature branch properly"() {

        given:
        def sut = new DynamicMatcher(FEATURE_PATTERN, "")

        expect:
        sut.matches(branch) == expected

        where:
        branch                 || expected
        "feature/"             || false
        "feature/2021.21"      || false
        "feature/2021.21a"     || false
        "feature/proj-1982376" || true
        "feature/x-1"          || true
    }

    def "Substitutes feature branch properly"() {

        given:
        def sut = new DynamicMatcher(FEATURE_PATTERN, "f\$1")

        when:
        def result = sut.replaced(branch)

        then:
        result == expected

        where:
        branch                 || expected
        "feature/"             || "feature/"
        "feature/proj-1982376" || "f1982376"
        "feature/x-1"          || "f1"
    }

    def "Leaves superfluous backreferences alone"() {

        given:
        def sut = new DynamicMatcher(FEATURE_PATTERN, "0=\$0;1=\$1;2=\$2;3=\$3")

        when:
        def result = sut.replaced("feature/wob-123")

        then:
        !result
//        result.contains("0=\$0")
//        result.contains("1=123")
//        result.contains("2=\$2")
//        result.contains("3=\$3")
    }

    def "Handles repeated backreferences"() {

        given:
        def sut = new DynamicMatcher(
            "(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d+)",
            "1=\$1;12=\$12;10=\$10;11=1")

        when:
        def result = sut.replaced("12345678901234567890")

        then:
        result.contains("1=1")
        result.contains("10=0")
        result.contains("11=1")
        result.contains("12=234567890")
    }

}
