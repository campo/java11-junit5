package nl.marimba.euler

import spock.lang.Specification

class Euler1Spec extends Specification {

    def "test p1sol1"() {
        given:
        def sut = new Euler1(1000);
        System.out.printf("The sum of all the multiples of 3 or 5 below %d is %d\n",
            sut.getMax(), sut.p1sol2());

        expect:
        sut.p1sol1() == sut.p1sol2()
    }
}
