package nl.marimba.euler

import spock.lang.Shared
import spock.lang.Specification

import nl.marimba.strings.XmlStripper

class MatcherSpec extends Specification {

    @Shared
    def xmlBody = """
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
</project>
"""

    @Shared
    def xmlDocDQ = """<?xml version="1.0" encoding="UTF-8"?>""" + xmlBody

    @Shared
    def xmlDocDQSO = """<?xml version="1.0" encoding="UTF-8" standalone="no" ?>""" + xmlBody

    @Shared
    def xmlDocSQ = """<?xml version='1.0' encoding='UTF-8' >  """ + xmlBody

    def "remove declaration from the beginning of the document"() {

        when:
        def stripped = XmlStripper.removeXmlDeclaration(xmlDoc)

        then:
        stripped.startsWith("<project")

        where:
        xmlDoc << [xmlDocDQ, xmlDocDQSO, xmlDocSQ]
    }

}
