import nl.marimba.kotlin.io.SerializeJson
import spock.lang.Specification

class KotlinJsonSpec extends Specification {

    def k = new SerializeJson()

    def "Serializing a plain object to Json"() {

        when:
        def s = k.buildSimple()

        then:
        s == $/{"tns":"hello world","version":"1.2.3"}/$
    }

    def "Serializing an array of objects to Json"() {

        when:
        def s = k.buildList()

        then:
        s == $/[{"tns":"hello first three worlds","version":"1.2.3"},{"tns":"hello worlds","version":"4.5.6"}]/$
    }

}
