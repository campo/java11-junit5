import spock.lang.Specification

class HelloKtSpec extends Specification {

    def k = new HelloKotlin()

    def blup() {

        when:
        k.sayHi("bloop2")

        then:
        true
    }

}
