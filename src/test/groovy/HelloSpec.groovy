import spock.lang.Specification

class Hello extends Specification {

    def blup() {

        given:
        def x = 3

        expect:
        x == Integer.parseInt("3")
    }

}
