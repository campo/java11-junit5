package nl.marimba.euler;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class Euler2Test {

    @Test
    void testFib() {
        assertThat(Fibonacci.nextFib.apply(1, 2).getR()).isEqualTo(3);
        assertThat(Fibonacci.nextFib.apply(1, 2).getL()).isEqualTo(2);
        assertThat(Fibonacci.nextFib.apply(3, 5).getL()).isEqualTo(5);
        assertThat(Fibonacci.nextFib.apply(3, 5).getR()).isEqualTo(8);
    }

    @Test
    void testFibReeksSimple() {
        Fibonacci fib = new Fibonacci();
        assertThat(fib.getAsInt()).isEqualTo(1);
        assertThat(fib.getAsInt()).isEqualTo(2);
        assertThat(fib.getAsInt()).isEqualTo(3);
        assertThat(fib.getAsInt()).isEqualTo(5);
        assertThat(fib.getAsInt()).isEqualTo(8);
        assertThat(fib.getAsInt()).isEqualTo(13);
    }

    @Test
    void testFibReeksSerie() {
        Fibonacci fib = new Fibonacci();

        List<Integer> result = IntStream.generate(fib)
                .takeWhile((i) -> i < 1_000_000 )
                .peek((i) -> System.out.printf("%d, ", i))
                .filter((i) -> i % 2 == 0)
                .peek((i) -> System.out.printf("\n%5d\n", i))
                .boxed()
                .collect(Collectors.toList());
        assertThat(result.get(3)).isEqualTo(144);
    }

    @Test
    void testProblem2() {
        assertThat(Euler2.p2sol1(9)).isEqualTo(10);
        assertThat(Euler2.p2sol1(50)).isEqualTo(44);
        assertThat(Euler2.p2sol1(200)).isEqualTo(188);
        assertThat(Euler2.p2sol1(1000)).isEqualTo(798);
        assertThat(Euler2.p2sol1(10_000)).isEqualTo(3382);
        assertThat(Euler2.p2sol1(1_000_000)).isEqualTo(1089154);
    }
}