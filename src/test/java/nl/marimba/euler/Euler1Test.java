package nl.marimba.euler;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class Euler1Test {

    @Test
    void verifyResults() {
        Euler1 sut = new Euler1(1000);
        assertThat(sut.p1sol1()).isEqualTo(sut.p1sol2());
        System.out.printf("The sum of all the multiples of 3 or 5 below %d is %d\n",
                sut.getMax(), sut.p1sol2());
    }

}